import type { List, ListItem } from "@prisma/client";
import { Button } from "primereact/button";
import { useState } from "react";
import { InputTextarea } from "primereact/inputtextarea";
import { api } from "@/utils/api";
import DisplayListItems from "../DisplayListItems";
import { InputText } from "primereact/inputtext";

type Props = {
  list: List;
};

export default function BoardColumn({ list }: Props) {
  const [showForm, setShowForm] = useState<boolean>(false);
  const [title, setTitle] = useState<string>("");
  const [description, setDescription] = useState("");
  const [listItemsState, setListItemsState] = useState<ListItem[]>([]);

  const { data: listItemData, refetch: listItemsRefetch } =
    api.listItem.getAllListItems.useQuery(
      {
        listId: list.id,
      },
      {
        onSuccess(data: ListItem[]) {
          setListItemsState(data);
        },
      }
    );

  const createItem = api.listItem.createListItem.useMutation({
    onSuccess() {
      listItemsRefetch().catch((err) => console.error(err));
    },
  });

  function handleSubmit(e: React.SyntheticEvent) {
    e.preventDefault();

    if (!title || !description) return;

    createItem.mutate({ listId: list.id, title, description });
    setDescription("");
    setTitle("");
    setShowForm(false);
  }

  return (
    <div className="w-[270px] rounded bg-slate-100 p-4 shadow-md dark:bg-gray-800 dark:text-white">
      <div className="flex items-center justify-between gap-4">
        <h4>{list.title}</h4>
        <span>
          <i className="pi pi-book"></i>
        </span>
      </div>

      {listItemData && listItemsState && listItemsState.length > 0 && (
        <DisplayListItems
          setListItemsState={setListItemsState}
          listId={list.id}
          listItems={listItemsState}
        />
      )}
      {!showForm && (
        <Button
          className="h-10 w-[80%]"
          label="add a task"
          icon="pi pi-plus"
          onClick={() => setShowForm(true)}
        />
      )}
      {showForm && (
        <form
          onSubmit={handleSubmit}
          className="flex flex-col items-center gap-3"
        >
          <InputText
            placeholder="item title"
            type="text"
            onChange={(e) => setTitle(e.target.value)}
            className="mb-2 w-[100%]"
          />
          <InputTextarea
            value={description}
            onChange={(e) => setDescription(e.target.value)}
            rows={5}
            cols={30}
            className="h-24 w-[100%] "
            placeholder="Your task description"
          />
          <div className="mt-2 flex w-full justify-between gap-2 ">
            <Button
              className="h-10 w-[50%]"
              label="add"
              icon="pi pi-check"
              type="submit"
              outlined
              severity="success"
            />
            <Button
              className="h-10 w-[50%]"
              severity="danger"
              label="X"
              type="submit"
              onClick={() => setShowForm(false)}
              text
            />
          </div>
        </form>
      )}
    </div>
  );
}
