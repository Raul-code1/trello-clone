/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { useState } from "react";
import { Button } from "primereact/button";
import { Dialog } from "primereact/dialog";
import {
  type RefetchQueryFilters,
  type RefetchOptions,
  type QueryObserverBaseResult,
} from "@tanstack/react-query";
import { InputText } from "primereact/inputtext";
import { api } from "@/utils/api";

type Props = {
  label: string;
  refetchWorkspaces: <TPageData>(
    options?: RefetchOptions & RefetchQueryFilters<TPageData>
  ) => Promise<QueryObserverBaseResult<any, any>>;
  refetchBoards: <TPageData>(
    options?: RefetchOptions & RefetchQueryFilters<TPageData>
  ) => Promise<QueryObserverBaseResult<any, any>>;
  workspaceId: string;
};

export default function CreateBoardModal(props: Props) {
  const createBoard = api.board.createBoard.useMutation({
    onSuccess() {
      props.refetchBoards().catch((err) => console.error(err));
    },
  });

  const [boardName, setBoardName] = useState("");
  const [visible, setVisible] = useState<boolean>(false);
  const footerContent = (
    <div>
      <Button
        label="Create Board"
        icon="pi pi-check"
        onClick={() => {
          if (!boardName) return;
          setVisible(false);
          createBoard.mutate({
            workspaceId: props.workspaceId,
            boardName,
          });
        }}
        autoFocus
        severity="success"
        outlined
      />
    </div>
  );

  return (
    <div className="card justify-content-center flex">
      <Button label={props.label} onClick={() => setVisible(true)} />
      <Dialog
        header="Create a Board"
        visible={visible}
        onHide={() => setVisible(false)}
        footer={footerContent}
        draggable={false}
        className="w-[90%] sm:w-[50%] "
      >
        <div className="card justify-content-center flex">
          <InputText
            placeholder="Board name"
            onChange={(e) => setBoardName(e.target.value)}
          />
        </div>
      </Dialog>
    </div>
  );
}
