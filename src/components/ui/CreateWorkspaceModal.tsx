/* eslint-disable @typescript-eslint/no-misused-promises */
/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { useState } from "react";
import { Dialog } from "primereact/dialog";
import { Button } from "primereact/button";
import { InputText } from "primereact/inputtext";
import { api } from "@/utils/api";
import {
  type RefetchQueryFilters,
  type RefetchOptions,
  type QueryObserverBaseResult,
} from "@tanstack/react-query";
import { signOut } from "next-auth/react";

type Props = {
  label: string;
  refetch: <TPageData>(
    options?: RefetchOptions & RefetchQueryFilters<TPageData>
  ) => Promise<QueryObserverBaseResult<any, any>>;
};

export default function CreateWorkspaceModal(props: Props) {
  const [inputValue, setInputValue] = useState("");
  const [visible, setVisible] = useState<boolean>(false);

  const createWorkspace = api.workspace.createWorkspace.useMutation({
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    onSuccess: (data) => {
      props.refetch().catch((err) => console.log(err));
    },
    onError: (err) => {
      console.log(err.message);
    },
  });

  const footerContent = (
    <div>
      <Button
        label="Close"
        icon="pi pi-times"
        onClick={() => setVisible(false)}
        severity="danger"
        outlined
      />
      <Button
        label="Create"
        icon="pi pi-check"
        onClick={() => {
          setVisible(false);
          createWorkspace.mutate({ name: inputValue });
          setInputValue("");
        }}
        outlined
      />
    </div>
  );

  const show = () => {
    setVisible(true);
  };

  return (
    <div className="  sm:pt-6">
      <div className="flex items-center  justify-between gap-4">
        <Button
          label={props.label}
          onClick={() => show()}
          severity="secondary"
          outlined
          className="h-12 w-[50%] sm:w-[20%]"
        />
        <button
          className="hidden rounded bg-blue-500 px-4 py-2 font-semibold text-white hover:bg-blue-600 sm:block "
          onClick={() => signOut().catch((err) => console.log(err))}
        >
          Sign out
        </button>
      </div>

      <Dialog
        header="Create Workspace"
        visible={visible}
        position={"center"}
        className="w-[90vw] sm:w-[50vw] "
        onHide={() => setVisible(false)}
        footer={footerContent}
        draggable={false}
        resizable={false}
      >
        <span className="p-float-label">
          <InputText
            id="workspace"
            value={inputValue}
            onChange={(e) => setInputValue(e.target.value)}
          />
          <label htmlFor="workspace">Workspace Name</label>
        </span>
      </Dialog>
    </div>
  );
}
