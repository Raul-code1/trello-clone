import React from "react";
import { Menubar } from "primereact/menubar";
import { type MenuItem } from "primereact/menuitem";
import { signOut } from "next-auth/react";
import {  type Workspace } from "@prisma/client";

type Props = {
  workspaces: Workspace[];
  handleSelectedWorkspace(workspace: Workspace): void;
};

export default function NavMenuMobile(props: Props) {
  /* function getWorkSpaceBoards(boards: Board[]): MenuItem[] {
    return boards.map((board) => {
      return { id: board.id, label: board.boardName };
    });
  } */

  const menuItems: MenuItem[] = props.workspaces.map((workspace) => {
    return {
      label: workspace.name,
      id: workspace.id,
      command: () => props.handleSelectedWorkspace(workspace),
    };
  });

  const items: MenuItem[] = [
    {
      label: "Workspaces",
      icon: "pi pi-fw pi-file",
      items: menuItems,
    },
    {
      label: "Logout",
      icon: "pi pi-sign-out",
      command: () => {
        signOut().catch((err) => console.error(err));
      },
    },
  ];

  return (
    <div className="card sm:hidden">
      <Menubar model={items} />
    </div>
  );
}
