/* eslint-disable @typescript-eslint/no-explicit-any */
import { api } from "@/utils/api";
import type {
  QueryObserverBaseResult,
  RefetchOptions,
  RefetchQueryFilters,
} from "@tanstack/react-query";
import { Button } from "primereact/button";
import { InputText } from "primereact/inputtext";
import { useState } from "react";

type Props = {
  btnText: string;
  boardId: string;
  listRefetch: <TPageData>(
    options?: RefetchOptions & RefetchQueryFilters<TPageData>
  ) => Promise<QueryObserverBaseResult<any, any>>;
};

export default function CreateBoardList({
  btnText,
  boardId,
  listRefetch,
}: Props) {
  const [showInput, setShowInput] = useState<boolean>(false);
  const [inputValue, setInputValue] = useState<string>("");

  const createList = api.list.createList.useMutation({
    onSuccess() {
      listRefetch().catch((err) => console.log(err));
    },
  });

  function handleSubmit(e: React.SyntheticEvent) {
    e.preventDefault();
    console.log(inputValue);
    if (!inputValue) return;

    createList.mutate({ boardId, title: inputValue });
    setShowInput(false);
  }

  return (
    <section className="absolute bottom-3 right-0">
      {!showInput && (
        <Button label={btnText} onClick={() => setShowInput(true)} />
      )}
      {showInput && (
        <form className="mt-6" onSubmit={handleSubmit}>
          <InputText
            onChange={(e) => setInputValue(e.target.value)}
            type="text"
            placeholder="Your list name"
          />
          <div>
            <Button label="Create" type="submit" />
            <Button
              label="X"
              severity="danger"
              text
              onClick={() => {
                setShowInput(false);
                setInputValue("");
              }}
            />
          </div>
        </form>
      )}
    </section>
  );
}
