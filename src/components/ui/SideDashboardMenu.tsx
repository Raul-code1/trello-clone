import React from "react";
import { PanelMenu } from "primereact/panelmenu";
import { type MenuItem } from "primereact/menuitem";

import { type Workspace } from "@prisma/client";

type Props = {
  workspaces: Workspace[];
  handleSelectedWorkspace(workspace: Workspace): void;
  activeWorkspace: Workspace;
};

export default function SideDashboardMenu(props: Props) {
  //TODO: Boards request?

  /*  function getWorkSpaceBorads(boards: Board[]): MenuItem[] {
    return boards.map((board) => {
      return { id: board.id, label: board.boardName };
    });
  } */

  const menuItems: MenuItem[] = props.workspaces.map((workspaceInfo) => {
    return {
      label: workspaceInfo.name,
      id: workspaceInfo.id,
      command: () => props.handleSelectedWorkspace(workspaceInfo),
      disabled: workspaceInfo.id === props.activeWorkspace.id,
    };
  });

  /* const items: MenuItem[] = [
    {
      label: "File",
      icon: "pi pi-fw pi-file",
      items: [
        {
          label: "New",
          icon: "pi pi-fw pi-plus",
          items: [
            {
              label: "Bookmark",
              icon: "pi pi-fw pi-bookmark",
            },
            {
              label: "Video",
              icon: "pi pi-fw pi-video",
            },
          ],
        },
        {
          label: "Delete",
          icon: "pi pi-fw pi-trash",
        },
        {
          label: "Export",
          icon: "pi pi-fw pi-external-link",
        },
      ],
    },
  ]; */
  return (
    <div className="card  hidden sm:block sm:w-[30%]">
      <PanelMenu model={menuItems} className="md:w-25rem w-full" />
    </div>
  );
}
