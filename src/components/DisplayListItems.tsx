/* eslint-disable @typescript-eslint/no-unsafe-argument */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import type { ListItem } from "@prisma/client";
import type { Dispatch, SetStateAction } from "react";
import { Draggable, Droppable } from "react-beautiful-dnd";

type Props = {
  listId: string;
  listItems: ListItem[];
  setListItemsState: Dispatch<SetStateAction<ListItem[]>>;
};

export default function DisplayListItems({
  listId,
  listItems,
  setListItemsState,
}: Props) {
  return (
    <Droppable droppableId={listId} direction="vertical" type="ITEMS">
      {(provided) => (
        <div
          className="flex min-w-full flex-col  gap-7"
          ref={provided.innerRef}
          {...provided.droppableProps}
        >
          {listItems.map((item, index) => (
            <Draggable draggableId={item.id} index={index} key={item.id}>
              {(provided) => (
                <div
                  className="bg-gray-700"
                  ref={provided.innerRef}
                  {...provided.draggableProps}
                  {...provided.dragHandleProps}
                >
                  <div>
                    <h1>{item.itemTitle}</h1>
                  </div>
                </div>
              )}
            </Draggable>
          ))}
          {provided.placeholder}
        </div>
      )}
    </Droppable>
  );
}
