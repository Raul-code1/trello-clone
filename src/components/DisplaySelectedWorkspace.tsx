/* eslint-disable @typescript-eslint/no-explicit-any */
import { type Workspace } from "@prisma/client";
import randomColor from "randomcolor";

import CreateBoardModal from "./ui/CreateBoardModal";
import {
  type RefetchQueryFilters,
  type RefetchOptions,
  type QueryObserverBaseResult,
} from "@tanstack/react-query";
import { api } from "@/utils/api";
import Link from "next/link";
import { truncateString } from "@/utils/helpers";

type Props = {
  workspace: Workspace;
  refetchWorkspaces: <TPageboards>(
    options?: RefetchOptions & RefetchQueryFilters<TPageboards>
  ) => Promise<QueryObserverBaseResult<any, any>>;
};

export default function DisplaySelectedWorkspace({
  workspace,
  refetchWorkspaces,
}: Props) {
  const { data: boards, refetch: refetchBoards } =
    api.board.getAllBoards.useQuery({
      workspaceId: workspace.id,
    });

  return (
    <div>
      {/* TODO: Edit workspace name */}
      <h1 className="pb-4 text-4xl">{workspace.name}</h1>
      <hr />
      <div className="grid grid-cols-2 gap-3 px-2 py-4 sm:grid-cols-4 ">
        {boards &&
          boards.length > 0 &&
          boards.map((board) => {
            // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment, @typescript-eslint/no-unsafe-call
            const color = randomColor({
              luminosity: "light",
              hue: "blue",
            });
            return (
              <Link
                href={`/workspace/${board.workspaceId}`}
                key={board.id}
                style={{
                  // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
                  border: `2px solid ${color}`,
                }}
                className=" cursor-pointer rounded-md p-6 shadow-md transition-all hover:bg-gray-200"
              >
                <h3>{truncateString(board.boardName, 22)}</h3>
              </Link>
            );
          })}
      </div>

      <CreateBoardModal
        label={
          boards && boards.length === 0
            ? "Create your first board"
            : "Create a new board"
        }
        refetchWorkspaces={refetchWorkspaces}
        workspaceId={workspace.id}
        refetchBoards={refetchBoards}
      />
    </div>
  );
}
