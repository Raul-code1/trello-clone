import { useSession } from "next-auth/react";
import { useRouter } from "next/router";
import { useEffect } from "react";
import Spinner from "./ui/Spinner";

type Props = {
  children: React.ReactNode;
};

export const ProtectedLayout = ({ children }: Props): JSX.Element => {
  const router = useRouter();
  const { status: sessionStatus } = useSession();
  const authorized = sessionStatus === "authenticated";
  const unAuthorized = sessionStatus === "unauthenticated";
  const loading = sessionStatus === "loading";

  useEffect(() => {
    if (loading || !router.isReady) return;

    if (unAuthorized) {
      router
        .push({
          pathname: "/",
          query: { returnUrl: router.asPath },
        })
        .catch((err) => console.error(err));
    }
  }, [loading, unAuthorized, sessionStatus, router]);

  if (loading) {
    return <Spinner />;
  }

  return authorized ? (
    <main className="relative mx-auto  box-border h-screen max-w-screen-xl bg-[]  ">
      {children}
    </main>
  ) : (
    <></>
  );
};
