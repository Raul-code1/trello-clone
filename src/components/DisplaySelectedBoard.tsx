/* eslint-disable @typescript-eslint/no-unsafe-argument */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import type { Board, List } from "@prisma/client";
import CreateBoardList from "./ui/CreateBoardList";
import { truncateString } from "@/utils/helpers";
import { api } from "@/utils/api";
import Spinner from "./ui/Spinner";
import { DragDropContext, Draggable, Droppable } from "react-beautiful-dnd";
import { useState } from "react";
import BoardColumn from "./ui/BoardColumn";

type Props = {
  selectedBoard: Board;
};

export default function DisplaySelectedBoard({ selectedBoard }: Props) {
  const [listState, setListState] = useState<List[]>([]);

  const {
    data: listData,
    isLoading,
    refetch: listRefetch,
  } = api.list.getAllList.useQuery(
    {
      boardId: selectedBoard.id,
    },
    {
      onSuccess(data: List[]) {
        setListState(data);
      },
    }
  );

  function onDragEnd(result: any) {
    const { source, destination } = result;

    console.log(result);

    if (!destination) {
      return;
    }

    if (
      source.droppableId === destination.droppableId &&
      source.index === destination.index
    ) {
      return;
    }

    const updatedListState = Array.from(listState);
    const movedItem = updatedListState[source.index];
    updatedListState.splice(source.index, 1);
    updatedListState.splice(destination.index, 0, movedItem!);

    setListState(updatedListState);
  }

  return (
    <div className="min-h-full   sm:pl-[9rem]">
      <DragDropContext onDragEnd={onDragEnd}>
        <div className="min-h-[20%] md:flex md:items-center md:justify-between">
          <h1 className="text-3xl ">
            {truncateString(selectedBoard.boardName, 30)}
          </h1>
          <button className="cursor-pointer">
            <i className="pi pi-trash" style={{ color: "red" }}></i>
          </button>
        </div>

        <section className="flex   gap-6 overflow-x-auto ">
          {isLoading && <Spinner />}
          {/* Draggable */}

          {listData && listState && listState.length > 0 && (
            <Droppable
              droppableId={selectedBoard.id}
              direction="horizontal"
              type="LIST"
            >
              {(provided) => (
                <div
                  className="flex min-h-[80vh] min-w-full  gap-7"
                  ref={provided.innerRef}
                  {...provided.droppableProps}
                >
                  {listState.map((list, index) => (
                    <Draggable
                      draggableId={list.id}
                      index={index}
                      key={list.id}
                    >
                      {(provided) => (
                        <div
                          ref={provided.innerRef}
                          {...provided.draggableProps}
                          {...provided.dragHandleProps}
                        >
                          <BoardColumn list={list} />
                        </div>
                      )}
                    </Draggable>
                  ))}
                  {provided.placeholder}
                </div>
              )}
            </Droppable>
          )}

          {/* Draggable */}
          {"listItems" in selectedBoard ? (
            Array.isArray(selectedBoard.listItems) &&
            selectedBoard.listItems.length > 0 && (
              <CreateBoardList
                listRefetch={listRefetch}
                boardId={selectedBoard.id}
                btnText="Create a list"
              />
            )
          ) : (
            <CreateBoardList
              listRefetch={listRefetch}
              boardId={selectedBoard.id}
              btnText="Create your first list"
            />
          )}
        </section>
      </DragDropContext>
    </div>
  );
}
