/* eslint-disable @typescript-eslint/no-misused-promises */
import { useEffect, useState } from "react";

import NavMenuMobile from "@/components/ui/NavMenuMobile";
import SideDashboardMenu from "@/components/ui/SideDashboardMenu";
import { api } from "@/utils/api";
import { type Workspace } from "@prisma/client";
import { ProtectedLayout } from "@/components/ProtectedLayout";
import Spinner from "@/components/ui/Spinner";
import CreateWorkspaceModal from "@/components/ui/CreateWorkspaceModal";
import { useRouter } from "next/router";
import DisplaySelectedWorkspace from "@/components/DisplaySelectedWorkspace";

export default function DashboardPage() {
  const [workspaces, setWorkspaces] = useState<Workspace[]>();
  const [activeWorkspace, setActiveWorkspace] = useState<Workspace>();

  const router = useRouter();

  const {
    data,
    isLoading,
    refetch: refetchWorkspaces,
  } = api.workspace.getAllWorkSpaces.useQuery(undefined, {
    onSuccess: (data: Workspace[]) => {
      setWorkspaces(data);
    },
    onError: () => {
      router.push("/").catch((err) => console.log(err));
    },
  });

  function handleSelectedWorkspace(workspace: Workspace) {
    setActiveWorkspace(workspace);
  }
  useEffect(() => {
    if (data && data.length > 0) {
      setActiveWorkspace(data[0]);
    }
  }, [data]);

  if (isLoading) {
    return <Spinner />;
  }

  return (
    <ProtectedLayout>
      <NavMenuMobile
        handleSelectedWorkspace={handleSelectedWorkspace}
        workspaces={workspaces ? workspaces : []}
      />
      {workspaces && (
        <>
          <CreateWorkspaceModal
            label={
              workspaces.length <= 0
                ? "Create your first WorkSpace"
                : "Create workspace"
            }
            refetch={refetchWorkspaces}
          />
        </>
      )}
      {workspaces && workspaces.length <= 0 && (
        <h1>You do not have any workspace</h1>
      )}
      <section className="h-full  pt-10 sm:flex ">
        {data && activeWorkspace && workspaces && workspaces?.length > 0 && (
          <SideDashboardMenu
            handleSelectedWorkspace={handleSelectedWorkspace}
            workspaces={workspaces}
            activeWorkspace={activeWorkspace}
          />
        )}
        <div className="h-full w-full sm:pl-4">
          {activeWorkspace && (
            <DisplaySelectedWorkspace
              refetchWorkspaces={refetchWorkspaces}
              workspace={activeWorkspace}
            />
          )}
        </div>
      </section>
    </ProtectedLayout>
  );
}
