import DisplaySelectedBoard from "@/components/DisplaySelectedBoard";
import { ProtectedLayout } from "@/components/ProtectedLayout";
import SideBar from "@/components/ui/SideBar";
import Spinner from "@/components/ui/Spinner";
import { api } from "@/utils/api";
import type { Board } from "@prisma/client";
import { useRouter } from "next/router";
import { useState } from "react";

export default function WorkspacePage() {
  const { query } = useRouter();

  const [boardsState, setBoardsState] = useState<Board[]>();
  const [selectedBoard, setSelectedBoard] = useState<Board>();

  function handleSelectedBoard(board: Board) {
    setSelectedBoard(board);
  }

  const { data: workspace, isLoading } =
    api.workspace.getWorkspaceById.useQuery({
      workspaceId: query.id as string,
    });

  const { data: boards } = api.board.getAllBoards.useQuery(
    {
      workspaceId: query.id as string,
    },
    {
      onSuccess(data: Board[]) {
        setBoardsState(data);
      },
    }
  );
  if (isLoading) {
    return <Spinner />;
  }

  return (
    <ProtectedLayout>
      {boardsState && boards && workspace && (
        <SideBar
          // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
          boards={boardsState}
          workspaceName={workspace ? workspace.name : ""}
          handleSelectedBoard={handleSelectedBoard}
        />
      )}
      {selectedBoard ? (
        <DisplaySelectedBoard selectedBoard={selectedBoard} />
      ) : (
        <h1 className="text-5xl sm:pl-[9rem]">Select a board</h1>
      )}
    </ProtectedLayout>
  );
}
