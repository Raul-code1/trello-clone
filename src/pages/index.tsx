import { type NextPage } from "next";
import { signIn, signOut, useSession } from "next-auth/react";
import Head from "next/head";
import Link from "next/link";
import { useRouter } from "next/router";
import { Button } from "primereact/button";
import { useEffect, useState } from "react";

const Home: NextPage = () => {
  return (
    <>
      <Head>
        <title>Trello Clone</title>
        <meta
          name="description"
          content="Just trying to build a project management software for practice"
        />
      </Head>
      <main className="">
        <h1>Marketing page</h1>
        <AuthShowcase />
      </main>
    </>
  );
};

export default Home;

const AuthShowcase: React.FC = () => {
  const [isRedirecting, setIsRedirecting] = useState(false);
  const { data: sessionData } = useSession();
  const router = useRouter();

  useEffect(() => {
    if (sessionData && !isRedirecting && router.isReady) {
      setIsRedirecting(true);
      setTimeout(() => {
        router
          .push((router.query.returnUrl as string) || "/dashboard")
          .catch((err) => console.error(err));
      }, 1000);
    }
  }, [sessionData, isRedirecting, router]);

  return (
    <div>
      <p>{sessionData && <span>Logged in as {sessionData.user?.name}</span>}</p>
      <Button
        onClick={sessionData ? () => void signOut() : () => void signIn()}
      >
        {sessionData ? "Sign out" : "Sign in"}
      </Button>
      <br />
      {sessionData && <Link href={"/dashboard"}>Go to your dashboard</Link>}
    </div>
  );
};
