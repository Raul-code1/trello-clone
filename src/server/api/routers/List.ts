import { z } from "zod";
import { createTRPCRouter, protectedProcedure } from "@/server/api/trpc";

export const listRouter = createTRPCRouter({
  getAllList: protectedProcedure
    .input(z.object({ boardId: z.string() }))
    .query(async ({ input, ctx }) => {
      const List = await ctx.prisma.list.findMany({
        where: {
          boardId: input.boardId,
        },
      });

      return List;
    }),

  createList: protectedProcedure
    .input(z.object({ boardId: z.string(), title: z.string() }))
    .mutation(async ({ input, ctx }) => {
      const board = await ctx.prisma.list.create({
        data: {
          boardId: input.boardId,
          title: input.title,
        },
      });

      return board;
    }),
});
