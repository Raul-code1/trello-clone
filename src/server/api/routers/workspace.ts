import { z } from "zod";
import { createTRPCRouter, protectedProcedure } from "@/server/api/trpc";

export const workspaceRouter = createTRPCRouter({
  getAllWorkSpaces: protectedProcedure.query(async ({ ctx }) => {
    const workSpaces = await ctx.prisma.workspace.findMany({
      where: {
        userId: ctx.session.user.id,
      },
    });

    return workSpaces;
  }),

  getWorkspaceById: protectedProcedure
    .input(z.object({ workspaceId: z.string() }))
    .query(async ({ input, ctx }) => {
      const workspace = await ctx.prisma.workspace.findUnique({
        where: {
          id: input.workspaceId,
        },
      });

      return workspace;
    }),

  createWorkspace: protectedProcedure
    .input(z.object({ name: z.string() }))
    .mutation(async ({ input, ctx }) => {
      const workspace = await ctx.prisma.workspace.create({
        data: {
          userId: ctx.session.user.id,
          name: input.name,
        },
      });

      return workspace;
    }),
});
