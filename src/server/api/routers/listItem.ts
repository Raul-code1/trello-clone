import { z } from "zod";
import { createTRPCRouter, protectedProcedure } from "@/server/api/trpc";

export const listItemRouter = createTRPCRouter({
  getAllListItems: protectedProcedure
    .input(z.object({ listId: z.string() }))
    .query(async ({ input, ctx }) => {
      const ListItems = await ctx.prisma.listItem.findMany({
        where: {
          listId: input.listId,
        },
      });

      return ListItems;
    }),

  createListItem: protectedProcedure
    .input(
      z.object({
        listId: z.string(),
        title: z.string(),
        description: z.string(),
      })
    )
    .mutation(async ({ input, ctx }) => {
      const item = await ctx.prisma.listItem.create({
        data: {
          listId: input.listId,
          itemTitle: input.title,
          itemDescription: input.description,
        },
      });

      return item;
    }),
});
