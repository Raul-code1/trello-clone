import { z } from "zod";
import { createTRPCRouter, protectedProcedure } from "@/server/api/trpc";

export const boardRouter = createTRPCRouter({
  getAllBoards: protectedProcedure
    .input(z.object({ workspaceId: z.string() }))
    .query(async ({ input, ctx }) => {
      const boards = await ctx.prisma.board.findMany({
        where: {
          workspaceId: input.workspaceId,
        },
      });

      return boards;
    }),

  createBoard: protectedProcedure
    .input(z.object({ boardName: z.string(), workspaceId: z.string() }))
    .mutation(async ({ input, ctx }) => {
      const board = await ctx.prisma.board.create({
        data: {
          boardName: input.boardName,
          workspaceId: input.workspaceId,
        },
      });

      return board;
    }),
});
