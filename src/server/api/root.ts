import { exampleRouter } from "@/server/api/routers/example";
import { createTRPCRouter } from "@/server/api/trpc";
import { workspaceRouter } from "./routers/workspace";
import { boardRouter } from "./routers/board";
import { listRouter } from "./routers/List";
import { listItemRouter } from "./routers/listItem";

/**
 * This is the primary router for your server.
 *
 * All routers added in /api/routers should be manually added here.
 */
export const appRouter = createTRPCRouter({
  example: exampleRouter,
  workspace: workspaceRouter,
  board: boardRouter,
  list: listRouter,
  listItem: listItemRouter,
});

// export type definition of API
export type AppRouter = typeof appRouter;
