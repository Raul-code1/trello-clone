# Trello

Just trying to implement workspaces, boards and boards items/task functionality like :[ Trello.com](https://trello.com/home)

This is a [T3 Stack](https://create.t3.gg/) project bootstrapped with `create-t3-app`.

Install dependencies:

```bash
npm install

```

```
Rename .env.example file to .env and add all the necessary environment variables
```

Before dev mode you want to make sure you have a database running locally, so hit the command:

```bash
docker compose up -d
```

And database url will look like this:

```bash
postgresql://postgres:example_password@localhost:5432/your-database-name
```

Run this command to create all data tables in the database:

```bash
npx prisma db push

```

You can manage the database either in **_localhost:8080_** docker container with docker compose file credentials or run prisma studio command:

```bash
npx prisma studio
```

For dev mode:

```bash
npm run dev

```

build:

```bash
npm run build

```

If you are not familiar with the different technologies used in this project, please refer to the respective docs. If you still are in the wind, please join our [Discord](https://t3.gg/discord) and ask for help.

- [Next.js](https://nextjs.org)
- [NextAuth.js](https://next-auth.js.org)
- [Prisma](https://prisma.io)
- [Tailwind CSS](https://tailwindcss.com)
- [tRPC](https://trpc.io)

## Learn More

To learn more about the [T3 Stack](https://create.t3.gg/), take a look at the following resources:

- [Documentation](https://create.t3.gg/)
- [Learn the T3 Stack](https://create.t3.gg/en/faq#what-learning-resources-are-currently-available) — Check out these awesome tutorials
